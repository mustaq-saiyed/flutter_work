import 'package:flutter/material.dart';

class ExpandedFlexible extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: 'OpenSans',
        primarySwatch: Colors.green,
        textTheme: ThemeData.light().textTheme.copyWith(
              title:  TextStyle(
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
            ),
        appBarTheme: AppBarTheme(
          textTheme: ThemeData.light().textTheme.copyWith(
                title: TextStyle(
                  fontFamily: 'OpenSans',
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
        ),
      ),
      title: 'Personal Expenses',
      home: ExpandedFlexiblePage(),
    );
  }
}

class ExpandedFlexiblePage extends StatefulWidget {
  @override
  _ExpandedFlexiblePageState createState() => _ExpandedFlexiblePageState();
}

class _ExpandedFlexiblePageState extends State<ExpandedFlexiblePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flexibale Expanded'),
      ),
      body: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Flexible(
                child: Container(
                  child: Text(
                    'Item 1 prety big',
                    textAlign: TextAlign.center,
                  ),
                  color: Colors.red,
                  height: 100,
                  width: 100,
                ),
              ),
              Flexible(
                flex: 5,
                fit: FlexFit.tight,
                child: Container(
                  child: Text(
                    'Item 2',
                    textAlign: TextAlign.center,
                  ),
                  color: Colors.blue,
                  height: 100,
                  width: 100,
                ),
              ),
              Flexible(
                flex: 1,
                fit: FlexFit.loose,
                child: Container(
                  child: Text(
                    'Item 3',
                    textAlign: TextAlign.center,
                  ),
                  color: Colors.orange,
                  height: 100,
                ),
              ),
            ],
          ),
          Row(
            children: [
              Expanded(
                flex: 1,
                child: Text(
                  "Expanded cover all screen ",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(backgroundColor: Colors.black12, fontSize: 15),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  "Expanded cover all screen ",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(backgroundColor: Colors.black12, fontSize: 15),
                ),
              ),
              Expanded(
                flex: 1,
                child: Text(
                  "Expanded cover all screen ",
                  textAlign: TextAlign.center,
                  style:
                      TextStyle(backgroundColor: Colors.black12, fontSize: 15),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}

import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:flutter_app/apiCall/ApiCall.dart';
import 'package:flutter_app/shopingApp/ShopingMainPage.dart';
import 'package:flutter_app/shopingApp/provider/Orders.dart';
import 'package:flutter_app/shopingApp/screen/EditProduct.dart';
import 'package:flutter_app/shopingApp/screen/OrderListingScreen.dart';
import 'package:flutter_app/shopingApp/screen/Product_Detail_Screen.dart';
import 'package:flutter_app/shopingApp/screen/CartScreeen.dart';
import 'package:flutter_app/shopingApp/screen/UserProduct.dart';
import 'package:provider/provider.dart';
import './provider/Card.dart';
import '../shopingApp/provider/Prducts.dart';

class ShoppingMain extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        //with value constructor
        ChangeNotifierProvider.value(
          value: Products(),
        ),
        ChangeNotifierProvider.value(
          value: Cart(),
        ),
        ChangeNotifierProvider(
          create: (ctx) => Orders(),
        )
        //without value constructor
        //ChangeNotifierProvider(create: (ctx) => Products())
        //ChangeNotifierProvider(create: (ctx)=>Cart());
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'F-cart 1',
        theme: ThemeData(
          fontFamily: 'OpenSans',
          primarySwatch: Colors.purple,
          errorColor: Colors.redAccent[700],
          textTheme: ThemeData.light().textTheme.copyWith(
              title: TextStyle(
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
                fontSize: 18,
              ),
              button: TextStyle(color: Colors.white)),
          appBarTheme: AppBarTheme(
            textTheme: ThemeData.light().textTheme.copyWith(
                  title: TextStyle(
                    fontFamily: 'OpenSans',
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  button: TextStyle(
                    color: Colors.white,
                  ),
                ),
          ),
        ),
        home: ApiCall(),
        routes: {
          ProductDetailsPage.routName: (ctx) => ProductDetailsPage(),
          CartScreen.routeName: (ctx) => CartScreen(),
          OrderListingScreen.routName:(ctx)=>OrderListingScreen(),
          UserProduct.routName:(ctx)=>UserProduct(),
          EditProductScreen.routeName:(ctx)=>EditProductScreen()

        },
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Prducts.dart';
import 'package:flutter_app/shopingApp/screen/EditProduct.dart';
import 'package:flutter_app/shopingApp/widget/AppDrawer.dart';
import 'package:flutter_app/shopingApp/widget/WidgetUserProductItem.dart';
import 'package:provider/provider.dart';

class UserProduct extends StatelessWidget {
  static const routName = '/userProduct';
  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<Products>(context, listen: true);

    final _appbar = AppBar(
      title: Text('Your Product'),
      actions: [
        IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context)
                  .pushReplacementNamed(EditProductScreen.routeName);
            })
      ],
    );
    final _body = Padding(
      padding: EdgeInsets.all(8),
      child: ListView.builder(
        itemCount: productData.items.length,
        itemBuilder: (_, index) => Column(
          children: [
            WidgetUserProductItem(
              productData.items[index].id,
              productData.items[index].title,
              productData.items[index].imageUrl,
            ),
            Divider(
              color: Colors.black38,
              height: 2,
            )
          ],
        ),
      ),
    );
    return Scaffold(
      appBar: _appbar,
      body: _body,
      drawer: AppDrawer(),
    );
  }
}

import 'dart:io' show Platform;
import 'dart:ui';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/shopingApp/provider/Orders.dart';
import '../widget/CartItem.dart';
import '../provider/Card.dart';

import 'package:provider/provider.dart';

class CartScreen extends StatelessWidget {
  static const routeName = '/cart';

  @override
  Widget build(BuildContext context) {
    final _cartItem = Provider.of<Cart>(context);
    double statusBarHeight = MediaQuery.of(context).padding.top +
        CupertinoNavigationBar().preferredSize.height;

    final _appBar = Platform.isIOS
        ? CupertinoNavigationBar(
            automaticallyImplyLeading: false,
            backgroundColor: Colors.black12,
            middle: Text(
              'Cart',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textScaleFactor: 1.1,
            ),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                Consumer<Cart>(
                  builder: (context, cart, child) => GestureDetector(
                    onTap: () {
                      //cartItem.clearAllCartItem();
                    },
                    child: Icon(CupertinoIcons.delete),
                  ),
                ),
              ],
            ),
          )
        : AppBar(
            title: Text(
              'Cart',
              style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
              textScaleFactor: 1.1,
            ),
          );

    final _noCartItemIn = Platform.isIOS
        ? CupertinoPageScaffold(
            child: Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      CupertinoIcons.cart,
                      size: 150,
                    ),
                    Text(
                      'No Item in Cart',
                      textScaleFactor: 1.1,
                      style: TextStyle(
                          fontSize: 20,
                          decoration: TextDecoration.none,
                          color: Colors.black54),
                    ),
                    CupertinoButton(
                      onPressed: () {
                        Navigator.of(context).pop('/');
                      },
                      child: Text(
                        'Continue to shopping..',
                        style:
                            TextStyle(fontSize: 25, color: Colors.deepOrange),
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        : Scaffold(
        appBar: _appBar,
          body: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Icon(
                  Icons.remove_shopping_cart_outlined,
                  size: 150,
                ),
                Text(
                  'No Item in Cart',
                  textScaleFactor: 1.1,
                  style: TextStyle(
                      fontSize: 20,
                      decoration: TextDecoration.none,
                      color: Colors.black54),
                ),
                CupertinoButton(
                  onPressed: () {
                    Navigator.of(context).pop('/');
                  },
                  child: Text(
                    'Continue to shopping..',
                    style:
                    TextStyle(fontSize: 25, color: Colors.deepOrange),
                  ),
                )
              ],
            ),
          ),
    );

    return Platform.isIOS
        ? CupertinoPageScaffold(
            navigationBar: _appBar,
            child: Padding(
              padding: EdgeInsets.only(top: statusBarHeight),
              child: _cartItem.cartItem.isEmpty
                  ? _noCartItemIn
                  : Column(
                      children: [
                        Card(
                          margin: EdgeInsets.all(15),
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                Row(
                                  children: [
                                    Image.network(''),
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text(
                                      'Total',
                                      style: TextStyle(
                                        fontSize: 20,
                                      ),
                                    ),
                                    Chip(
                                      label: Text(
                                        '\$ ${_cartItem.totalAmount.toStringAsFixed(2)}',
                                      ),
                                      backgroundColor: Theme.of(context)
                                          .primaryTextTheme
                                          .title
                                          .color,
                                    ),
                                    CupertinoButton(
                                      child: Text('Order Now'),
                                      onPressed: () {},
                                      padding: EdgeInsets.all(5),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
            ),
          )
        :
        _cartItem.cartItem.length!=0?
    Scaffold(
            appBar: _appBar,
            body: Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: _cartItem.cartItem.length,
                    itemBuilder: (ctx, index) => CartAddedItem(
                      _cartItem.cartItem.values
                          .toList()[index]
                          .id, //all the item ws in list so that's y user list.values.toList()[index]
                      _cartItem.cartItem.keys
                          .toList()[index], //al
                      _cartItem.cartItem.values.toList()[index].price,
                      _cartItem.cartItem.values.toList()[index].quantity,
                      _cartItem.cartItem.values.toList()[index].title,
                      _cartItem.cartItem.values.toList()[index].imaageUrl,
                      _cartItem.cartItem.values.toList()[index].descreption,


                    ),
                  ),
                ),


                Card(
                  margin: EdgeInsets.all(15),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Image.asset(""),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              'Total',
                              style: TextStyle(
                                fontSize: 20,
                              ),
                            ),
                            Chip(
                              label: Text(
                                '\$ ${_cartItem.totalAmount.roundToDouble()}',

                              ),
                              backgroundColor: Theme.of(context)
                                  .primaryTextTheme
                                  .title
                                  .color,
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    Provider.of<Orders>(context,listen: false).addOrder(_cartItem.cartItem.values.toList(),_cartItem.totalAmount);
                    _cartItem.clearAllCartItem();
                  },
                  child: Container(
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        color: Theme.of(context).primaryColor,
                        borderRadius: BorderRadius.circular(10)),
                    width: double.infinity,
                    child: Center(
                      heightFactor: 1.1,
                      widthFactor: 1.1,
                      child: Container(
                        margin: EdgeInsets.all(5),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Order Now',
                              textScaleFactor: 1.1,
                              softWrap: true,
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            Icon(
                              Icons.arrow_forward_outlined,
                              color: Colors.white,
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 1,
                ),
              ],
            ),
          ):_noCartItemIn;
  }
}

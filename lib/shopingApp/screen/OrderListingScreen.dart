import 'package:flutter/material.dart';
import '..//widget/AppDrawer.dart';
import '../provider/Orders.dart';
import '../widget/OrderItemList.dart';
import 'package:provider/provider.dart';

class OrderListingScreen extends StatelessWidget {

  static const routName='/orderListing';
  @override
  Widget build(BuildContext context) {
    final orderList = Provider.of<Orders>(context);

    final _appBar = AppBar(
      title: Text('Orders'),
    );
    final _body = ListView.builder(
      itemCount: orderList.orderList.length,
      itemBuilder: (ctx, index) => OrderItemsList(
        orderList.orderList[index],
      ),
    );
    return Scaffold(
      appBar: _appBar,
      body: _body,
      drawer: AppDrawer(),
    );
  }
}

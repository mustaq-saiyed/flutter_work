import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Prducts.dart';
import 'package:provider/provider.dart';

class ProductDetailsPage extends StatelessWidget {
  // final String title;
  // final double price;
  // ProductDetailsPage(this.title,this.price);

  static const routName = '/productDetails';

  @override
  Widget build(BuildContext context) {
    double _statusBarHeight =
        MediaQuery.of(context).padding.top + AppBar().preferredSize.height;
    final productId = ModalRoute.of(context).settings.arguments as String;
    final loadProduct = Provider.of<Products>(
      context,
      listen: false,
    ).findById(productId);

    print('Load Product ${loadProduct}');

    final _appBar = Platform.isIOS
        ? CupertinoNavigationBar(
            leading: GestureDetector(
              onTap: () {
                Navigator.of(context).pop('/');
              },
              child: Icon(CupertinoIcons.back),
            ),
            automaticallyImplyLeading: false,
            middle: Text(
              loadProduct.title,
              style: TextStyle(
                fontSize: 15,
                color: Colors.black,
                decoration: TextDecoration.none,
              ),
            ),
          )
        : AppBar(
            title: Text(
              loadProduct.title,
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          );

    final pageBody = Platform.isIOS
        ? CupertinoPageScaffold(
            navigationBar: _appBar,
            child: SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.only(top: _statusBarHeight),
                child: Column(
                  children: [
                    Image.network(
                      loadProduct.imageUrl,
                      height: MediaQuery.of(context).size.height - 200,
                      fit: BoxFit.fitHeight,
                      width: double.infinity,
                    ),
                    Text(
                      '\$ ${loadProduct.final_price.toString()}',
                      style: TextStyle(
                          fontSize: 15,
                          decoration: TextDecoration.none,
                          color: Colors.black45),
                      textScaleFactor: 1.1,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        loadProduct.description,
                        style: TextStyle(
                            fontSize: 15,
                            decoration: TextDecoration.none,
                            color: Colors.black45),
                        textScaleFactor: 1.1,
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        : Scaffold(
            appBar: _appBar,
            body: Container(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    Image.network(
                      loadProduct.imageUrl,
                      fit: BoxFit.cover,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          child: Text('\Rs.  ${ loadProduct.discount_price.toString()}'),
                        ),
                        Container(
                          margin: EdgeInsets.all(10),
                          padding: EdgeInsets.all(5),
                          decoration: BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.all(Radius.circular(10))
                          ),
                          child: Text(loadProduct.pricreOff),
                        )
                      ],
                    ),
                    Text(loadProduct.description)
                  ],
                ),
              ),
            ),
          );

    return pageBody;
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Product.dart';
import 'package:flutter_app/shopingApp/screen/UserProduct.dart';
import 'package:provider/provider.dart';
import '../provider/Prducts.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = '/editproduct';

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageUrlControler = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editProduct = Product(
      id: null,
      title: '',
      discount_price: 0,
      description: '',
      imageUrl: '',
      final_price: 0,
      pricreOff: '',
      isCart: false,
      isFavourite: false);

  var isInit = true;
  var _isLoading = false;
  var _initValues = {
    'title': '',
    'description': '',
    'price': '',
    'imageurl': ''
  };

  @override
  void initState() {
    // TODO: implement initState
    _imageUrlFocusNode.addListener((updateImageUrl));
    print('init Call');
    super.initState();
  }

  @override
  void didChangeDependencies() {
    print('didChangeDependencies');
    if (isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        _editProduct =
            Provider.of<Products>(context, listen: false).findById(productId);
        _initValues = {
          'title': _editProduct.title,
          'description': _editProduct.description,
          'price': _editProduct.discount_price.toString(),
          'imageUrl': ''
        };
        _imageUrlControler.text = _editProduct.imageUrl;
      }
    }
    isInit = false;
    super.didChangeDependencies();
  }

  void updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _imageUrlFocusNode.removeListener((updateImageUrl));
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageUrlControler.dispose();
    _imageUrlFocusNode.dispose();
  }

  void _saveForm() {
    final isValid = _form.currentState.validate();
    if (!isValid) {
      return;
    }
    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });
    if (_editProduct.id != null) {
      Provider.of<Products>(context, listen: false)
          .updateProduct(_editProduct.id, _editProduct);
      Navigator.of(context).pushReplacementNamed(UserProduct.routName);
      setState(() {
        _isLoading = false;
      });
    } else {
      Provider.of<Products>(context, listen: false)
          .addProduct(_editProduct)
          .catchError((onError) {
        showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('error occurred!'),
                  content: Text('Something went wrong'),
                  actions: [
                    FlatButton(
                      child: Text('OK'),
                      onPressed: () {
                        Navigator.of(ctx).pushReplacementNamed(UserProduct.routName);
                      },
                    )
                  ],
                ));
      }).then((_) {
        setState(() {
          _isLoading = false;
        });
        Navigator.of(context).pushReplacementNamed(UserProduct.routName);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    print(_isLoading);
    final _appbar = AppBar(
      leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pushReplacementNamed(UserProduct.routName);
        },
        child: Icon(Icons.arrow_back_sharp),
      ),
      title: Text("Edit Product"),
      actions: [],
    );

    final _body = Container(
      padding: EdgeInsets.all(15),
      child: Form(
        key: _form,
        child: ListView(
          children: [
            TextFormField(
              initialValue: _initValues['title'],
              decoration: InputDecoration(labelText: 'Title'),
              textInputAction: TextInputAction.next,
              onFieldSubmitted: (_) {
                FocusScope.of(context).requestFocus(_priceFocusNode);
              },
              validator: (edTitle) {
                if (edTitle.isEmpty) {
                  return 'Please enter title';
                }
                return null;
              },
              onSaved: (edTitle) {
                _editProduct = Product(
                  title: edTitle,
                  description: _editProduct.description,
                  discount_price: _editProduct.discount_price,
                  final_price: _editProduct.final_price,
                  id: _editProduct.id,
                  imageUrl: _editProduct.imageUrl,
                  pricreOff: _editProduct.pricreOff,
                  isFavourite: _editProduct.isFavourite,
                  isCart: _editProduct.isCart,
                );
              },
            ),
            TextFormField(
              initialValue: _initValues['price'],
              decoration: InputDecoration(labelText: 'Price'),
              keyboardType: TextInputType.number,
              textInputAction: TextInputAction.next,
              focusNode: _priceFocusNode,
              onFieldSubmitted: (_) {
                FocusScope.of(context).requestFocus(_descriptionFocusNode);
              },
              validator: (edPrice) {
                if (edPrice.isEmpty) {
                  return 'Please enetr price';
                }
                if (double.parse(edPrice) == null) {
                  return 'Please enetr valid price';
                }
                if (double.parse(edPrice) <= 0) {
                  return 'Please eneter valid price';
                }
                return null;
              },
              onSaved: (edPrice) {
                _editProduct = Product(
                  title: _editProduct.title,
                  description: _editProduct.description,
                  discount_price: double.parse(edPrice),
                  final_price: _editProduct.final_price,
                  id: _editProduct.id,
                  imageUrl: _editProduct.imageUrl,
                  pricreOff: _editProduct.pricreOff,
                  isFavourite: _editProduct.isFavourite,
                  isCart: _editProduct.isCart,
                );
              },
            ),
            TextFormField(
              initialValue: _initValues['description'],
              decoration: InputDecoration(labelText: 'Descreption '),
              textInputAction: TextInputAction.next,
              focusNode: _descriptionFocusNode,
              maxLines: 3,
              keyboardType: TextInputType.multiline,
              onFieldSubmitted: (_) {
                //FocusScope.of(context).requestFocus(_descriptionFocusNode);
              },
              validator: (edDescription) {
                if (edDescription.isEmpty) {
                  return 'Please enter description';
                }
                if (edDescription.length <= 10) {
                  return 'Description shout be more then 10 characters..';
                }
                return null;
              },
              onSaved: (edDescription) {
                _editProduct = Product(
                  title: _editProduct.title,
                  description: edDescription,
                  discount_price: _editProduct.discount_price,
                  final_price: _editProduct.final_price,
                  id: _editProduct.id,
                  imageUrl: _editProduct.imageUrl,
                  pricreOff: _editProduct.pricreOff,
                  isFavourite: _editProduct.isFavourite,
                  isCart: _editProduct.isCart,
                );
              },
            ),
            Column(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: Colors.grey),
                      ),
                      margin: EdgeInsets.only(top: 8, right: 10),
                      width: 80,
                      height: 80,
                      child: _imageUrlControler.text.isEmpty
                          ? Center(
                              child: Text(
                                'Enter Url',
                                textAlign: TextAlign.center,
                              ),
                            )
                          : FittedBox(
                              child: Image.network(
                                _imageUrlControler.text,
                                fit: BoxFit.cover,
                              ),
                            ),
                    ),
                    Expanded(
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Image Url',
                        ),
                        keyboardType: TextInputType.url,
                        textInputAction: TextInputAction.done,
                        controller: _imageUrlControler,
                        focusNode: _imageUrlFocusNode,
                        onFieldSubmitted: (_) {
                          _saveForm();
                        },
                        validator: (edUrl) {
                          if (edUrl.isEmpty) {
                            return 'Please enetr image url';
                          }
                          if (!edUrl.startsWith('http') &&
                              !edUrl.startsWith('https')) {
                            return 'Please enetr valid i  mage url';
                          }
                          if (!edUrl.endsWith('.png') &&
                              !edUrl.endsWith('.jpg') &&
                              !edUrl.endsWith('.jpeg')) {
                            return 'Please check url and enter valid image url';
                          }
                          return null;
                        },
                        onSaved: (edImageUrl) {
                          _editProduct = Product(
                            title: _editProduct.title,
                            description: _editProduct.description,
                            discount_price: _editProduct.discount_price,
                            final_price: _editProduct.final_price,
                            id: _editProduct.id,
                            imageUrl: edImageUrl,
                            pricreOff: _editProduct.pricreOff,
                            isFavourite: _editProduct.isFavourite,
                            isCart: _editProduct.isCart,
                          );
                        },
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(12),
                  ),
                  child: FlatButton(
                    textColor: Colors.white,
                    onPressed: () {
                      _saveForm();
                    },
                    child: Text('Submit'),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
    return Scaffold(
      appBar: _appbar,
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : _body,
    );
  }
}

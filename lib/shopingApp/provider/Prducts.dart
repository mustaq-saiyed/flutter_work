import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import './Product.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

class Products with ChangeNotifier {
  List<Product> _items = [
    Product(
      id: 'p1',
      title: 'HERE & NOW',
      description:
          'Men Black & Grey Slim Fit Printed Cuban Collar Casual Shirt',
      discount_price: 879.99,
      final_price: 1599.00,
      pricreOff: '45%OFF',
      imageUrl:
          'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/11629122/2020/3/17/d30ea7db-275f-418c-a5d1-49584cd84dba1584436320637-HERENOW-Men-Shirts-1641584436318543-3.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Bene Kleed',
      description: 'Men Off-White & Blue Slim Fit Printed Casual Shirt.',
      discount_price: 734.99,
      final_price: 2099.00,
      pricreOff: '65%OFF',
      imageUrl:
          'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/7189947/2018/8/30/7d6de012-d553-418d-99b6-eb28c945fb7f1535614137786-Bene-Kleed-Men-Off-White--Blue-Slim-Fit-Printed-Casual-Shirt-3181535614137565-3.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Dennis Lingo',
      description: 'Men Pink Slim Fit Solid Casual Shirt.',
      discount_price: 591.99,
      final_price: 1849.00,
      pricreOff: '68%OFF',
      imageUrl:
          'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/7488102/2019/8/22/8002a744-0dad-4dbb-9481-cf0090134c3b1566454086786-Dennis-Lingo-Men-Pink-Slim-Fit-Solid-Casual-Shirt-9891566454-1.jpg',
    ),
    Product(
      id: 'p4',
      title: 'Dennis Lingo',
      description: 'Men Olive Green Slim Fit Solid Casual Shirt.',
      discount_price: 591.99,
      final_price: 1848.00,
      pricreOff: '68%OFF',
      imageUrl:
          'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/7488103/2019/8/22/b7868593-94f4-4b6e-97de-931733a74a0f1566454100581-Dennis-Lingo-Men-Green-Slim-Fit-Solid-Casual-Shirt-358156645-5.jpg',
    ),
    // Product(
    //   id: 'p5',
    //   title: 'Roadster',
    //   description: 'Prepare any meal you want.',
    //   discount_price: 584.99,
    //   final_price: 1299.00,
    //   pricreOff: '55%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/8894769/2019/4/11/a0f6b650-9dba-4c0e-adfe-240165351c071554963598079-Roadster-Men-Shirts-1461554963596098-5.jpg',
    // ),
    // Product(
    //   id: 'p6',
    //   title: 'Calvin Klein Jeans',
    //   description:
    //       'Men White & Black Regular Fit Tropical Printed Cotton Linen Shirt',
    //   discount_price: 2999.99,
    //   final_price: 5999.00,
    //   pricreOff: '50%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/11371134/2020/9/10/173311b6-bf3b-4814-898d-05620fcaf3ea1599728069358-Calvin-Klein-Jeans-Men-Shirts-2391599728068069-3.jpg',
    // ),
    // Product(
    //   id: 'p7',
    //   title: 'Campus Sutra',
    //   description: 'Men Red & Black Standard Regular Fit Checked Casual Shirt.',
    //   discount_price: 559,
    //   final_price: 1399.00,
    //   pricreOff: '60%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/10017031/2019/6/20/29e7dd2e-7da2-422e-9f64-446b3e7937ed1561014422802-Campus-Sutra-Men-Red--Black-Regular-Fit-Checked-Casual-Shirt-3.jpg',
    // ),
    // Product(
    //   id: 'p8',
    //   title: 'HERE&NOW',
    //   description:
    //       'Men Olive Green & Black Slim Fit Camouflage Printed Hooded Casual Shirt.',
    //   discount_price: 599.99,
    //   final_price: 999.00,
    //   pricreOff: '30%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/10922084/2019/12/12/b1c7cd09-f974-4536-870f-1109dfb6953b1576141891214-HERENOW-Men-Olive-Green--Black-Slim-Fit-Printed-Casual-Shirt-7.jpg',
    // ),
    // Product(
    //   id: 'p9',
    //   title: 'Roadster',
    //   description: 'Men Blue Regular Fit Solid Casual Shirt.',
    //   discount_price: 749.99,
    //   final_price: 999.00,
    //   pricreOff: '25%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/11896086/2020/6/13/797096e5-6923-4a71-8504-fdbcf927b90d1592039912394-Roadster-Men-Shirts-2811592039910686-6.jpg',
    // ),
    // Product(
    //   id: 'p10',
    //   title: 'Roadste',
    //   description: 'Men Blue Slim Fit Faded Casual Denim Shirt.',
    //   discount_price: 1199.99,
    //   final_price: 1599.00,
    //   pricreOff: '30%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/2284633/2018/2/19/11519031607748-Roadster-Men-Shirts-5971519031607655-5.jpg',
    // ),
    // Product(
    //   id: 'p11',
    //   title: 'Jack & Jones',
    //   description: 'Men Black Printed Slim Fit Round Neck T-shirt.',
    //   discount_price: 749.99,
    //   final_price: 1499.00,
    //   pricreOff: '50%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/11206690/2020/2/26/4759eb11-a8e5-47ce-bd34-be98241772d21582698825318-Jack--Jones-Men-Tshirts-8571582698823689-5.jpg',
    // ),
    // Product(
    //   id: 'p12',
    //   title: 'Kook N Keech Marvel',
    //   description: 'White Civil War Print T-shirt.',
    //   discount_price: 389.99,
    //   final_price: 599.00,
    //   pricreOff: '35%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/1293766/2016/4/19/11461062179888-Kook-N-Keech-Marvel-White-Printed-T-shirt-9111461062179657-5.jpg',
    // ),
    // Product(
    //   id: 'p13',
    //   title: 'Donald Duck',
    //   description: 'Men Yellow Printed Round Neck T-shirt',
    //   discount_price: 421.99,
    //   final_price: 649.00,
    //   pricreOff: '35%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/1439758/2018/1/4/11515056822801-Donald-Duck-Men-Yellow-Printed-Round-Neck-T-shirt-9861515056822684-2.jpg',
    // ),
    // Product(
    //   id: 'p14',
    //   title: 'HERE&NOW',
    //   description: 'Men Grey & Black Abstract Printed Round Neck T-shirt.',
    //   discount_price: 449.99,
    //   final_price: 749.00,
    //   pricreOff: '40%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/4318147/2018/5/4/11525435102230-HERENOW-Men-Black-Abstract-Printed-Round-Neck-T-shirt-1231525435102048-1.jpg',
    // ),
    // Product(
    //   id: 'p15',
    //   title: 'GRITSTONES',
    //   description: 'Men Black Solid High Neck T-shirt.',
    //   discount_price: 428.99,
    //   final_price: 1299.00,
    //   pricreOff: '67%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/productimage/2020/9/5/38d89af8-675c-4b5a-983e-21d6b3507dc61599258365170-1.jpg',
    // ),
    // Product(
    //   id: 'p16',
    //   title: 'Moda Rapido',
    //   description: 'Men Black & White Printed Round Neck T-shirt.',
    //   discount_price: 454.99,
    //   final_price: 649.00,
    //   pricreOff: '30%OFF',
    //   imageUrl:
    //       'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/2378361/2018/11/28/8434c1b7-4c03-4509-97cc-c9f7ee7b43721543382292897-Moda-Rapido-Men-White--Black-Printed-Round-Neck-T-shirt-8561543382292124-1.jpg',
    // ),
  ];

  var _showFavouritOnly = false;

  List<Product> get items {
    // if (_showFavouritOnly) {
    //   return _items.where((element) => element.isFavourite).toList();
    // }
    return [..._items];
  }

  List<Product> get favourite {
    return _items.where((element) => element.isFavourite).toList();
  }

  // void showFavoirute() {
  //   _showFavouritOnly = true;
  //   notifyListeners();
  // }

  // void showAll() {
  //   _showFavouritOnly = false;
  //   notifyListeners();
  // }

  Product findById(String id) {
    return _items.firstWhere((element) => element.id == id);
  }

  Future<void> addProduct(Product productItem) async {
    const url = 'https://imperotraning-default-rtdb.firebaseio.com/products.json';
    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            'title': productItem.title,
            'description': productItem.description,
            'imageUrl': productItem.imageUrl,
            'discoount_price': productItem.discount_price,
            'final_price': productItem.final_price,
            'priceOff': productItem.pricreOff,
            'isFavorite': productItem.isFavourite,
            'isCart': productItem.isCart
          },
        ),
      );
      print(json.decode(response.body));
      final newProduct = Product(
        description: productItem.description,
        discount_price: productItem.discount_price,
        final_price: productItem.final_price,
        id: json.decode(response.body)['name'],
        imageUrl: productItem.imageUrl,
        pricreOff: productItem.pricreOff,
        title: productItem.title,
      );
      _items.add(newProduct);
      notifyListeners();
    } catch (error) {
      print(error);
      throw error;
    }
  }

  void updateProduct(String id, Product updatedProduct) {
    final prodIndex = _items.indexWhere((element) => element.id == id);
    if (prodIndex >= 0) {
      _items[prodIndex] = updatedProduct;
      notifyListeners();
    } else {
      print('Something went wrong');
    }
  }

  void deleteProduct(String id) {
    _items.removeWhere((element) => element.id == id);
    notifyListeners();
  }
}

import 'package:flutter/foundation.dart';
import 'package:flutter_app/shopingApp/provider/Card.dart';

class OrdersItems {
  final String id;
  final double amount;
  final List<CartItem> orderList;
  final DateTime dateTime;

  OrdersItems({@required this.id,
      @required this.amount,
      @required this.orderList,
      @required this.dateTime,});
}

class Orders with ChangeNotifier {
  List<OrdersItems> _ordeItemList = [];

  List<OrdersItems> get orderList {
    return [..._ordeItemList];
  }

  void addOrder(List<CartItem> cartItem, double total) {
    _ordeItemList.insert(
      0,
      OrdersItems(id: DateTime.now().toString(), amount: total, orderList: cartItem, dateTime: DateTime.now()));
    notifyListeners();
  }
}

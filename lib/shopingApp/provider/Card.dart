import 'package:flutter/widgets.dart';

import 'package:flutter/foundation.dart';

class CartItem {
  final String id;
  final String title;
  final int quantity;
  final double price;
  final String imaageUrl;
  final String descreption;
  bool isAddCart;

  CartItem(
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,
    @required this.imaageUrl,
    this.descreption,
    this.isAddCart,
  );
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _cartItem = {};
  Map<String, CartItem> get cartItem {
    return {..._cartItem};
  }

  int get cartCount {
    return _cartItem == null ? 0 : _cartItem.length;
  }

  String get imageUrl {}

  double get totalAmount {
    var total = 0.0;
    _cartItem.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void addItem(String productId, double price, String title, String imageUrl,String desc) {
    if (_cartItem.containsKey(productId)) {
      //update qty
      _cartItem.update(
        productId,
        (value) => CartItem(
            value.id,
            value.title,
            value.quantity + 1,
            value.price,
            value.imaageUrl,
            desc,
            false),
      );
    } else {
      _cartItem.putIfAbsent(
        productId,
        () => CartItem(
            DateTime.now().toString(), title, 1, price, imageUrl, desc, false),
      );
    }
    notifyListeners();
  }

  void clearAllCartItem() {
    _cartItem={};
    notifyListeners();
  }

  void removeSingleItemInCart(String id) {
    if (!_cartItem.containsKey(id)) {
      return;
    }
    if (_cartItem[id].quantity > 1) {
      _cartItem.update(
          id,
          (value) => CartItem(
              value.id,
              value.title,
              value.quantity - 1,
              value.price,
              value.imaageUrl,
              value.descreption,
              value.isAddCart));
    } else {
      _cartItem.remove(id);
    }
    notifyListeners();
  }

  void removeItem(String productId){
    _cartItem.remove(productId);
    notifyListeners();
  }

  void addToCart() {}
}
//199

import 'package:flutter/foundation.dart';

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double discount_price;
  final double final_price;
  final String pricreOff;
  final String imageUrl;
  bool isFavourite;
  bool isCart;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.discount_price,
    @required this.final_price,
    @required this.pricreOff,
    @required this.imageUrl,
    this.isFavourite=false,
    this.isCart=false,
  });

  void toggleFavouriteStatus() {
    isFavourite = !isFavourite;
    notifyListeners();
  }

  void toggleAddCart(){
    isCart=!isCart;
    notifyListeners();
  }
}

import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import './provider/Card.dart';
import './screen/CartScreeen.dart';
import './widget/SimpelTextView.dart';
import './widget/Badge.dart';
import 'package:provider/provider.dart';
import 'widget/AppDrawer.dart';
import 'widget/ProductGrid.dart';

enum FilterOption { Favourits, All }

class ShpingMainPage extends StatefulWidget {
  @override
  _ShpingMainPageState createState() => _ShpingMainPageState();
}

class _ShpingMainPageState extends State<ShpingMainPage> {
  var _showOnlyFavourites = false;

  @override
  Widget build(BuildContext context) {
    final _body = ProductsGrid(_showOnlyFavourites);
    final _appBar = Platform.isIOS //for ios user CupertinoNavigationBar
        ? CupertinoNavigationBar(
            middle: Text(''),
            trailing: Row(
              mainAxisSize: MainAxisSize.min,
              children: [
                GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(CupertinoIcons.shopping_cart),
                  ),
                  onTap: () {
                    Navigator.of(context).pushNamed(CartScreen.routeName);
                  },
                ),
                GestureDetector(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: CupertinoContextMenu(
                      child: GestureDetector(
                        child: Icon(CupertinoIcons.ellipsis_vertical),
                        //for iso use CupertinoIcons
                        onTap: () {
                          print('Context Menu');
                        },
                      ),
                      actions: <Widget>[
                        CupertinoContextMenuAction(
                          onPressed: () {},
                          child: const Text(
                            'All',
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 15,
                                color: Colors.black54),
                            textScaleFactor: 1.1,
                          ),
                        ),
                        CupertinoContextMenuAction(
                          onPressed: () {},
                          child: const Text(
                            'Fav',
                            style: TextStyle(
                                decoration: TextDecoration.none,
                                fontSize: 15,
                                color: Colors.black54),
                            textScaleFactor: 1.1,
                          ),
                        )
                      ],
                    ),
                  ),
                )
              ],
            ),
            leading: Row(
              children: [
                Text(
                  'F-Cart',
                  style: TextStyle(
                      fontSize: 23,
                      fontWeight: FontWeight.bold,
                      decoration: TextDecoration.none,
                      color: Colors.black),
                  textScaleFactor: 1.1,
                )
              ],
            ),
          )
        //for android use AppBar
        : AppBar(
            backgroundColor: Theme.of(context).primaryColor,
            title: SimpelTextView(
              'F-Cart',
              Colors.white,
              20,
            ),
            actions: [
              PopupMenuButton(
                onSelected: (FilterOption selctedValue) {
                  setState(() {
                    if (selctedValue == FilterOption.Favourits) {
                      _showOnlyFavourites = true;
                    } else {
                      _showOnlyFavourites = false;
                    }
                  });
                },
                icon: Icon(Icons.more_vert), //normal icon for a
                itemBuilder: (_) => [
                  PopupMenuItem(
                    child: Text('Only Faoutits'),
                    value: FilterOption.Favourits,
                  ),
                  PopupMenuItem(
                    child: Text('Show All'),
                    value: FilterOption.All,
                  ),
                ],
              ),
              Consumer<Cart>(
                builder: (_, cartData, _child) => Badge(
                  child: _child,
                  value: cartData.cartCount.toString(),
                ),
                child: IconButton(
                  onPressed: () {
                    Navigator.of(context).pushNamed(CartScreen.routeName);
                  },
                  icon: Icon(
                    Icons.shopping_cart,
                  ),
                ),
              )
            ],
          );

    return Platform.isIOS
        ? CupertinoPageScaffold(
            child: _body,
            navigationBar: _appBar,
          )
        : Scaffold(
            appBar: _appBar,
            body: _body,
            drawer: AppDrawer(),
          );
  }
}

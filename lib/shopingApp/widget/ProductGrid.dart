import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Prducts.dart';
import 'package:provider/provider.dart';

import 'Product_Items.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavoutit;
  ProductsGrid(this.showFavoutit);
  @override
  Widget build(BuildContext context) {
    final productData = Provider.of<Products>(context);
    final productList =showFavoutit ? productData.favourite : productData.items;

    final _body=GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 2,
        childAspectRatio: 2 / 2,
      ),
      itemCount: productList.length,
      itemBuilder: (mCtx, index) => ChangeNotifierProvider.value(
        //create: (c) => productList[index],
        value: productList[index],
        child: ProductItem(),
      ),
    );

    return Platform.isIOS ? CupertinoPageScaffold(
      child: _body,
    ): _body;
  }
}

import 'dart:io' show Platform;
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Card.dart';
import 'package:flutter_svg/flutter_svg.dart';
import '../provider/Product.dart';
import '../screen/Product_Detail_Screen.dart';
import '../widget/SimpelTextView.dart';
import 'package:provider/provider.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cartProvider = Provider.of<Cart>(context, listen: false);
    /*timeDilation=5.0;*/

    final _bodyAndroid = Container(
      width: double.infinity,
      margin: EdgeInsets.all(2),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(15),
        child: GridTile(
          header: Container(
            height: 35,
            color: Colors.black54,
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
              child: SimpelTextView(
                product.title,
                Colors.white,
                20.00,
              ),
            ),
          ),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pushNamed(ProductDetailsPage.routName,
                  arguments: product.id);
            },
            child: Image.network(
              product.imageUrl,
              fit: BoxFit.fill,
            ),
          ),
          footer: GridTileBar(
            backgroundColor: Colors.black87,
            leading: Consumer<Product>(
              builder: (context, product, child) => IconButton(
                icon: Icon(
                  product.isFavourite ? Icons.favorite : Icons.favorite_border,
                  color: Colors.yellow,
                  size: 20,
                ),
                onPressed: () {
                  product.toggleFavouriteStatus();
                },
                color: Theme.of(context).accentColor,
              ),
            ),
            title: Container(
              child: Row(
                children: [
                  // SimpelTextView(
                  //   'Rs. ${originalPrice.toString()}',
                  //   Colors.white,
                  //   17.00,
                  // ),
                  Text(
                    'Rs. ${product.discount_price.toString()}',
                    overflow: TextOverflow.ellipsis,
                    style: TextStyle(
                      decoration: TextDecoration.none,
                      fontSize: 12,
                    ),
                  ),
                ],
              ),
            ),
            trailing: Consumer<Product>(
              builder: (context, product, child) => IconButton(
                icon: Icon(
                  product.isCart
                      ? Icons.shopping_cart
                      : Icons.shopping_cart_outlined,
                  size: 18,
                ),
                onPressed: () {
                  Scaffold.of(context).hideCurrentSnackBar();
                  cartProvider.addItem(product.id, product.discount_price,
                      product.title, product.imageUrl,product.description);
                  Scaffold.of(context).showSnackBar(
                    SnackBar(
                      content: Text('Item added'),
                      backgroundColor: Theme.of(context).primaryColor,
                      elevation: 5.5,
                      action: SnackBarAction(
                        textColor: Colors.yellow,
                        label: 'Undo', onPressed: () {
                            cartProvider.removeSingleItemInCart(product.id);
                      },
                      ),

                    ),
                  );
                },
                color: Colors.yellow,
              ),
            ),
          ),
        ),
      ),
    );

    final _bodyIso = SafeArea(
      child: Container(
        width: double.infinity,
        margin: EdgeInsets.all(5),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: GridTile(
            header: Container(
                /*height: 35,
              color: Colors.black54,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                child: Text(
                  product.title,
                  style: TextStyle(fontSize: 20,decoration: TextDecoration.none,color: Colors.white,),

                ),
              ),*/
                ),
            child: GestureDetector(
              onTap: () {
                Navigator.of(context).pushNamed(ProductDetailsPage.routName,
                    arguments: product.id);
              },
              child: Image.network(
                product.imageUrl,
                fit: BoxFit.fill,
              ),
            ),
            footer: GridTileBar(
              backgroundColor: Colors.black87,
              leading: Consumer<Product>(
                builder: (context, product, child) => GestureDetector(
                  onTap: () {
                    product.toggleFavouriteStatus();
                    print('Ios Fav');
                  },
                  child: SvgPicture.asset(
                    product.isFavourite
                        ? 'assets/svg/like.svg'
                        : 'assets/svg/heart.svg',
                    color: Colors.yellow,
                    height: 25,
                    width: 25,
                  ),
                ),
              ),
              title: Text(
                'Rs. ${product.discount_price.toString()}',
                textScaleFactor: 1.1,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  decoration: TextDecoration.none,
                  fontSize: 5,
                ),
              ),
              trailing: Consumer<Product>(
                builder: (context, product, child) => GestureDetector(
                  onTap: () {
                    cartProvider.addItem(product.id, product.discount_price,
                        product.title, product.imageUrl,product.description);
                    product.toggleAddCart();
                    //showAlertDialog(context,product.isCart);
                    print('Ios Cart ${product.isCart}');
                  },
                  child: Icon(
                    product.isCart
                        ? CupertinoIcons.cart_fill
                        : CupertinoIcons.shopping_cart,
                    color: Colors.yellow,
                    size: 25,
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );

    return Platform.isIOS
        ? CupertinoPageScaffold(
            child: _bodyIso,
          )
        : _bodyAndroid;
  }
}

import 'package:flutter/material.dart';

class SimpelTextView extends StatelessWidget {
  final String _title;
  final Color _textColor;
  final double _textSize;
  

  SimpelTextView(
    this._title,
    this._textColor,
    this._textSize,
  
  );

  @override
  Widget build(BuildContext context) {
    return Text(
      _title,
      style: TextStyle(
        color: _textColor,
        fontSize: _textSize,
      ),
      textAlign: TextAlign.justify,
    );
  }
}

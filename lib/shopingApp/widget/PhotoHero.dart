import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HeroWidgets extends StatelessWidget {

  final String tag;
  final String title;
  final String descreption;
  final String price;
  final String imageUrl;
  final double width;

  HeroWidgets(this.title, this.descreption, this.price, this.imageUrl, this.width, this.tag);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: Hero(
        tag: tag,
        child: Material(
          child: InkWell(
            onTap: (){
              Navigator.of(context).pop();
            },child: Image.asset(imageUrl,fit: BoxFit.cover,),
          ),
        ),
      )
      ,
    );
  }
}

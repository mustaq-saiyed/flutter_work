import 'package:flutter/material.dart';

class TextViewRounded extends StatelessWidget {
  final String _title;
  final Color _textColor;
  final double _textSize;
  

  TextViewRounded(
    this._title,
    this._textColor,
    this._textSize,
  
  );

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
      
      ),
      child: Text(
        _title,
        style: TextStyle(
          color: _textColor,
          fontSize: _textSize,
        ),
        textAlign: TextAlign.justify,
      ),
    );
  }
}

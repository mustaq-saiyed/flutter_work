

import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Prducts.dart';
import 'package:flutter_app/shopingApp/screen/EditProduct.dart';
import 'package:provider/provider.dart';

class WidgetUserProductItem extends StatelessWidget {
  final String id;
  final String title;
  final String image;

  WidgetUserProductItem(this.id,this.title, this.image);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      title: Text('$title'),
      leading: CircleAvatar(
        backgroundImage: NetworkImage(image),
      ),
      trailing: Container(
        width: 100,
        child: Row(
          children: [
            IconButton(
                icon: Icon(
                  Icons.edit,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  Navigator.of(context).pushReplacementNamed(EditProductScreen.routeName,arguments: id);
                }),
            IconButton(
                icon: Icon(
                  Icons.delete,
                  color: Colors.red,
                ),
                onPressed: () {
                  Provider.of<Products>(context,listen: false).deleteProduct(id);
                })
          ],
        ),
      ),
    );
  }
}

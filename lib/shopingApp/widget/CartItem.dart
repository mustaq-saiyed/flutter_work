import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Card.dart';
import 'package:provider/provider.dart';

class CartAddedItem extends StatelessWidget {
  final String id;
  final String productid;
  final double price;
  final int qty;
  final String title;
  final String imageUrl;
  final String desc;

  CartAddedItem(
    this.id,
    this.productid,
    this.price,
    this.qty,
    this.title,
    this.imageUrl,
    this.desc,
  );

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.endToStart,
      confirmDismiss: (direction) {
        return showDialog(
            context: context,
            builder: (ctx) => AlertDialog(
                  title: Text('Are you sure'),
                  content: Text('Do you want to remove item from cart'),
                  actions: [
                    FlatButton(
                      child: Text('No'),
                      onPressed: () {
                        Navigator.of(context).pop(false);
                      },
                    ),
                    FlatButton(
                      child: Text('Yes'),
                      onPressed: () {
                        Navigator.of(context).pop(true);
                      },
                    ),
                  ],
                ));
      },
      onDismissed: (direction) {
        Provider.of<Cart>(context, listen: false).removeItem(productid);
      },
      key: ValueKey(id),
      background: Container(
        color: Colors.red,
        child: Icon(
          Icons.delete,
          color: Colors.white,
          size: 40,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(
          right: 20,
        ),
        margin: EdgeInsets.symmetric(
          vertical: 12,
          horizontal: 5,
        ),
      ),
      child: Card(
        margin: EdgeInsets.symmetric(
          vertical: 10,
          horizontal: 4,
        ),
        child: Padding(
          padding: EdgeInsets.all(8),
          child: ListTile(
            leading: ClipOval(
              child: Image.network(
                imageUrl,
                width: 50,
                height: 90,
                fit: BoxFit.cover,
              ),
            ),
            title: Text(title),
            subtitle: Text('Total: \$${price * qty}'),
            trailing: Text('$qty X'),
          ),
        ),
      ),
    );
  }
}

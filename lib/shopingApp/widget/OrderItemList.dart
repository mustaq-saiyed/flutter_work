import 'dart:math';
import 'package:flutter/material.dart';
import 'package:flutter_app/shopingApp/provider/Orders.dart';
import 'package:intl/intl.dart';

class OrderItemsList extends StatefulWidget {
  final OrdersItems _ordersItems;

  OrderItemsList(this._ordersItems);

  @override
  _OrderItemsListState createState() => _OrderItemsListState();
}

class _OrderItemsListState extends State<OrderItemsList> {
  var _expanded = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      child: Column(
        children: [
          GestureDetector(
            onTap: (){
              setState(() {
                _expanded = !_expanded;
              });
            },
            child: ListTile(
              title: Text('\$ ${widget._ordersItems.amount.toStringAsFixed(2)}'),
              subtitle: Text(
                  DateFormat('dd/MM/yyyy').format(widget._ordersItems.dateTime)),
              trailing: IconButton(
                icon: Icon(_expanded ? Icons.expand_less : Icons.expand_more),
                onPressed: () {
                  setState(() {
                    _expanded = !_expanded;
                  });
                },
              ),
            ),
          ),
          if (_expanded)
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 15,vertical: 4),
              child: Container(
                height:
                    min(widget._ordersItems.orderList.length * 20.00 + 50, 150),
                child: ListView(
                    children: widget._ordersItems.orderList
                        .map((e) => Container(
                              margin: EdgeInsets.all(5),
                              child: Row(
                                children: [
                                  ClipOval(
                                    child: Image.network(
                                      e.imaageUrl,
                                      width: 50,
                                      height: 50,
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: 10, top: 5, bottom: 5),
                                    child: Text(
                                      e.title,
                                      style: TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                      ),
                                    ),
                                  ),
                                  Spacer(),
                                  Padding(
                                    padding: const EdgeInsets.only(right: 10),
                                    child: Text(
                                      '${e.quantity} x \$ ${e.price}',
                                      style: TextStyle(
                                        fontSize: 12,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.grey,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ))
                        .toList()),
              ),
            )
        ],
      ),
    );
  }
}

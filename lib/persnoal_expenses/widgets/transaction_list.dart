import 'package:flutter/material.dart';


import '../model/transaction.dart';
import 'transaction_items.dart';

class TranactionList extends StatelessWidget {
  final List<Transaction> _transaction;
  Function deleteTransaction;

  TranactionList(this._transaction, this.deleteTransaction);

  @override
  Widget build(BuildContext context) {
    print('Transaction Page Build');
    final mediaQueary = MediaQuery.of(context);
    return _transaction.isEmpty
        ? LayoutBuilder(builder: (mCtx, constrain) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    "No Transaction add yet!",
                    style: Theme.of(context).textTheme.title,
                    textAlign: TextAlign.center,
                  ),
                  Container(
                    height: constrain.maxHeight * 0.60,
                    child: Image.asset(
                      'assets/images/z.png',
                      fit: BoxFit.contain,
                    ),
                  )
                ],
              ),
            );
          })
        : ListView(
            children: _transaction.map(
              (value) => TransactionItem(
                key: ValueKey(value.id), 
                transaction: value,
                mediaQueary: mediaQueary,
                deleteTransaction: deleteTransaction,
              ),
            ).toList(),
          );
        
    // : ListView.builder(
    //     itemBuilder: (context, index) {
    //       return new TransactionItem(
    //         transaction: _transaction[index],
    //         mediaQueary: mediaQueary,
    //         deleteTransaction: deleteTransaction,
    //       );
    //     },
    //     itemCount: _transaction.length,
    //   );
  }
}

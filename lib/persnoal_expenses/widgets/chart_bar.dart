import 'package:flutter/material.dart';

class ChartBar extends StatelessWidget {
  final String lable;
  final double spendingAmount;
  final double spendingPctOcTotal;

  const ChartBar(
    this.lable,
    this.spendingAmount,
    this.spendingPctOcTotal,
  );

  @override
  Widget build(BuildContext context) {
    print('Chart Bar Page Call');
    return LayoutBuilder(builder: (mContext, constrain) {
      return Column(
        children: <Widget>[
          Container(
            height: constrain.maxHeight * 0.10,
            child: FittedBox(
              child: Text('\$${spendingAmount.toStringAsFixed(0)}'),
            ),
          ),
          SizedBox(
            height: constrain.maxHeight * 0.01,
          ),
          Container(
            height: constrain.maxHeight * 0.4,
            width: 15,
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(
                    border: Border.all(
                      color: Colors.grey,
                      width: 1.0,
                    ),
                    color: Color.fromRGBO(
                      220,
                      220,
                      220,
                      1,
                    ),
                    borderRadius: BorderRadius.circular(
                      10,
                    ),
                  ),
                ),
                FractionallySizedBox(
                  heightFactor: spendingPctOcTotal,
                  child: Container(
                    decoration: BoxDecoration(
                      color: Theme.of(context).primaryColor,
                      borderRadius: BorderRadius.circular(10),
                    ),
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: constrain.maxHeight * 0.04,
          ),
          Container(
            height: constrain.maxHeight * 0.12,
            child: FittedBox(
                child: Text(
              lable,
            )),
          ),
        ],
      );
    });
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_app/persnoal_expenses/model/transaction.dart';
import 'package:flutter_app/persnoal_expenses/widgets/chart_bar.dart';
import 'package:intl/intl.dart';

class Chart extends StatelessWidget {
  final List<Transaction> recentTransaction;

  Chart(this.recentTransaction){
    print('Constructor Chart');
  }

  List<Map<String, Object>> get dayTransactionValue {
    return List.generate(7, (index) {
      final weekDay = DateTime.now().subtract(
        Duration(days: index),
      );

      var totalSum = 0.0;

      for (var i = 0; i < recentTransaction.length; i++) {
        if (recentTransaction[i].dateTime.day == weekDay.day &&
            recentTransaction[i].dateTime.month == weekDay.month &&
            recentTransaction[i].dateTime.year == weekDay.year) {
          totalSum = recentTransaction[i].amount;
        }
      }
      return {
        'day': DateFormat.E().format(weekDay).substring(0, 1),
        'amount': totalSum,
      };
    }).reversed.toList();
  }

  double get maxSpending {
    return dayTransactionValue.fold(0.0, (sum, item) {
      return sum + item['amount'];
    });
  }

  @override
  Widget build(BuildContext context) {
    print('Chart Page Call');
    
    return Card(
      elevation: 10,
      margin: EdgeInsets.all(10),
      child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: dayTransactionValue.map((element) {
            return Flexible(
              fit: FlexFit.loose,
              child:   ChartBar(
                element['day'],
                element['amount'],
                maxSpending == 0.0
                    ? 0.0
                    : (element['amount'] as double) / maxSpending,
              ),
            );
          }).toList()),
    );
  }
}

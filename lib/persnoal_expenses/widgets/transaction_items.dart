import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_app/persnoal_expenses/model/transaction.dart';
import 'package:intl/intl.dart';

class TransactionItem extends StatefulWidget {
  const TransactionItem({
    Key key,
    @required this.transaction,
    @required this.mediaQueary,
    @required this.deleteTransaction,
  }) : super(key: key);
 
  final Transaction transaction;
  final MediaQueryData mediaQueary;
  final Function deleteTransaction;

  @override
  _TransactionItemState createState() => _TransactionItemState();
}

class _TransactionItemState extends State<TransactionItem> {
  Color _bgColor;
  @override
  void initState() {
    const availableColor = [
      Colors.red,
      Colors.blue,
      Colors.pink,
      Colors.amber,
    ];
    _bgColor = availableColor[Random().nextInt(availableColor.length)];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        elevation: 5,
        margin: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        child: ListTile(
          leading: Padding(
            padding: const EdgeInsets.all(6),
            child: CircleAvatar(
              backgroundColor: _bgColor,
              child: Padding(
                padding: EdgeInsets.all(6),
                child: FittedBox(
                  child: Text('\$${widget.transaction.amount}'),
                ),
              ),
            ),
          ),
          title: Text(
            widget.transaction.title,
            style: Theme.of(context).textTheme.title,
          ),
          subtitle: Text(
            DateFormat.yMMMEd().format(widget.transaction.dateTime),
          ),
          trailing: widget.mediaQueary.size.width > 460
              ? FlatButton.icon(
                  icon: Icon(
                    Icons.delete,
                  ),
                  label: const Text(
                    'Delete',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  textColor: Theme.of(context).errorColor,
                  onPressed: () =>
                      widget.deleteTransaction(widget.transaction.id))
              : IconButton(
                  onPressed: () =>
                      widget.deleteTransaction(widget.transaction.id),
                  icon: Icon(
                    Icons.delete,
                    color: Theme.of(context).errorColor,
                  ),
                ),
        ),
      ),
    );
  }
}

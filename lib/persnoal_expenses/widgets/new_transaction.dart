import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class NewTransaction extends StatefulWidget {
  final Function addTranaction;
  NewTransaction(this.addTranaction) {
    print("New Transaction Consturct Call");
  }
  @override
  _NewTransactionState createState() {
    print("Create State NewTransaction Widget");
    return _NewTransactionState();
  }
}

class _NewTransactionState extends State<NewTransaction> {
  final _titleControler = TextEditingController();
  final _amountControler = TextEditingController();
  DateTime _selcetedDate;

  _NewTransactionState() {
    print('Constructor NewTransaction State');
  }

  @override
  void initState() {
    print('Init State');
    super.initState();
  }

  @override
  void didUpdateWidget(covariant NewTransaction oldWidget) {
    print('didupdateWidget');
    super.didUpdateWidget(oldWidget);
  }
  @override
  void dispose() {
    print('Dispose ');
    super.dispose();
  }

  void _submitTransaction() {
    if (_amountControler.text.isEmpty) {
      return;
    }
    final enterTititle = _titleControler.text;
    final enterAmount = double.parse(_amountControler.text);
    if (enterTititle.isEmpty || enterAmount <= 0 || _selcetedDate == null) {
      return;
    }

    widget.addTranaction(
      enterTititle,
      enterAmount,
      _selcetedDate,
    );
    Navigator.of(context).pop();
  }

  void _getDatePicker() {
    showDatePicker(
            context: context,
            initialDate: DateTime.now(),
            firstDate: DateTime(2019),
            lastDate: DateTime.now())
        .then((pickDate) {
      if (pickDate == null) {
        return;
      }
      setState(() {
        _selcetedDate = pickDate;
      });
    });
    print('');
  }

  @override
  Widget build(BuildContext context) {
    print('New Transaction Page Call');
    final mediaQueary = MediaQuery.of(context);
    return SingleChildScrollView(
      child: Card(
        elevation: 5,
        child: Container(
          padding: EdgeInsets.only(
            top: 10,
            left: 10,
            right: 10,
            bottom: mediaQueary.viewInsets.bottom + 10,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: TextField(
                  onSubmitted: (_) => _submitTransaction,
                  controller: _titleControler,
                  decoration: InputDecoration(
                      alignLabelWithHint: true,
                      labelText: 'Title',
                      hintText: 'Title'),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(left: 10),
                child: TextField(
                  onSubmitted: (_) => _submitTransaction,
                  keyboardType: TextInputType.number,
                  controller: _amountControler,
                  decoration: InputDecoration(
                      alignLabelWithHint: true,
                      hintText: 'Amount',
                      labelText: 'Amount'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 10),
                child: Container(
                  height: 70,
                  child: Row(
                    children: [
                      Expanded(
                        child: Text(
                          _selcetedDate == null
                              ? 'No Date Chosen!'
                              : 'Pick Date is :- ${DateFormat.yMMMd().format(_selcetedDate)}',
                          style: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      FlatButton(
                        textColor: Theme.of(context).primaryColor,
                        child: Text('Choos Date',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            )),
                        onPressed: _getDatePicker,
                      )
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(
                  horizontal: 10,
                ),
                child: RaisedButton(
                  textColor: Theme.of(context).textTheme.button.color,
                  child: Text(
                    'Add Transaction',
                    style: TextStyle(
                      color: Colors.purple,
                    ),
                    textAlign: TextAlign.end,
                  ),
                  onPressed: _submitTransaction,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter_app/mealApp/widgets/main_drawer.dart';

class FilterScreen extends StatefulWidget {
  static const rountName = '/filters';

  final Function saveFilter;

  final Map<String, bool> currentFilter;
  FilterScreen(this.currentFilter, this.saveFilter);

  @override
  _FilterScreenState createState() => _FilterScreenState();
}

class _FilterScreenState extends State<FilterScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegen = false;
  bool _lactoseFree = false;

  @override
  void initState() { 
    _glutenFree=widget.currentFilter['gluten'];
    _vegetarian=widget.currentFilter['vagetarain'];
    _vegen=widget.currentFilter['vegan'];
    _lactoseFree=widget.currentFilter['lactose'];
    super.initState();
  }

  Widget _switchList(
    String title,
    String subTitle,
    bool currentValue,
    Function updateVale,
  ) {
    return SwitchListTile(
      title: Text(title),
      value: currentValue,
      subtitle: Text(subTitle),
      onChanged: updateVale,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Your Filter'),
        actions: [
          IconButton(
            icon: Icon(
              Icons.save,
              color: Colors.white,
            ),
            onPressed: () {
              final mySelctedFilter = {
                'gluten': _glutenFree,
                'lactose': _lactoseFree,
                'vegan': _vegen,
                'vegtarian': _vegetarian
              };
              widget.saveFilter(mySelctedFilter);
            },
          )
        ],
      ),
      drawer: MainDrawer(),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text(
              'Adjust Y cour\n   Meal Selection',
              style: TextStyle(
                fontSize: 25,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Expanded(
            child: ListView(
              children: [
                _switchList(
                  'Gluten-free',
                  'Only includ Gluten-free meal',
                  _glutenFree,
                  (newValue) {
                    setState(
                      () {
                        _glutenFree = newValue;
                      },
                    );
                  },
                ),
                _switchList('Lactose-free', 'Only includ Lactose-free meal',
                    _lactoseFree, (newValue) {
                  setState(
                    () {
                      _lactoseFree = newValue;
                    },
                  );
                }),
                _switchList(
                  'Vagetrain',
                  'Only includ Vagetrain-free meal',
                  _vegetarian,
                  (newValue) {
                    setState(
                      () {
                        _vegetarian = newValue;
                      },
                    );
                  },
                ),
                _switchList(
                  'Vegan',
                  'Only includ Vegin-free meal',
                  _vegen,
                  (newValue) {
                    setState(
                      () {
                        _vegen = newValue;
                      },
                    );
                  },
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

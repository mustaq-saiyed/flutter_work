import 'package:flutter/material.dart';

import './categories_screen.dart';
import './favourites_screen.dart';
import '../model/meal.dart';
import '../widgets/main_drawer.dart';

class TabsScreen extends StatefulWidget {
  final List<Meal> favouriteMeals;

  TabsScreen(this.favouriteMeals);

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
 List<Map<String, Object>> _pagesList;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pagesList=[
    {
      'page': CategoriesScreen(),
      'title': 'Categories',
    },
    {
      'page': FavouritesScreen(widget.favouriteMeals),
      'title': 'Favourites',
    }
  ];
    super.initState();
  }

  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(_pagesList[_selectedPageIndex]['title']),
        ),
        drawer: Drawer(
          child: MainDrawer(),
        ),
        body: _pagesList[_selectedPageIndex]['page'],
        bottomNavigationBar: bottomNavBar(_selectedPageIndex, _selectPage)
        //default navigation
        // BottomNavigationBar(
        //   onTap: _selectPage,
        //   backgroundColor: Theme.of(context).primaryColor,
        //   items: [
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.category),
        //       title: Text('Categories'),
        //     ),
        //     BottomNavigationBarItem(
        //       icon: Icon(Icons.star),
        //       title: Text('Favourites'),
        //     )
        //   ],
        // ),

        );
  }

  //widget create
  Widget bottomNavBar(int index, Function function) {
    return BottomNavigationBar(
      onTap: function,
      backgroundColor: Theme.of(context).primaryColor,
      unselectedItemColor: Colors.white,
      selectedItemColor: Theme.of(context).accentColor,
      currentIndex: _selectedPageIndex,
      selectedFontSize: 12,
      type: BottomNavigationBarType.shifting,
      items: [
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.category,
          ),
          title: Text('Categories', style: TextStyle()),
        ),
        BottomNavigationBarItem(
          backgroundColor: Theme.of(context).primaryColor,
          icon: Icon(
            Icons.star,
          ),
          title: Text(
            'Favourites',
            style: TextStyle(),
          ),
        )
      ],
    );
  }

  //default tab controler
  Widget defaulTabControlder() {
    return DefaultTabController(
      length: 2,
      initialIndex: 0, //default tab to load just like 0 1 2 and show on
      child: Scaffold(
        appBar: AppBar(
          title: Text('Mels'),
          bottom: TabBar(
            //manage tab order to load page
            tabs: <Widget>[
              Tab(
                icon: Icon(Icons.category),
                text: 'Category',
              ),
              Tab(
                icon: Icon(Icons.star),
                text: 'Favourites',
              ),
            ],
          ),
        ),
        body: TabBarView(
          children: [
            //load page order or tabbar tab
            CategoriesScreen(), FavouritesScreen(widget.favouriteMeals)
          ],
        ),
      ),
    );
  }
}

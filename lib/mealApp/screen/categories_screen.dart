import 'package:flutter/material.dart';
import 'package:flutter_app/mealApp/widgets/category_item.dart';

import '../dumy_data.dart';

class CategoriesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GridView(
      padding: const EdgeInsets.all(1),
      gridDelegate: SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 200,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
      ),
      children: DUMMY_CATEGORIES
          .map((categoryDate) => CategoryItem(
                categoryDate.title,
                categoryDate.color,
                categoryDate.id
              ))
          .toList(),
    );
  }
}

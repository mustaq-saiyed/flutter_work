import 'package:flutter/material.dart';
import 'package:flutter_app/mealApp/model/meal.dart';

import '..//widgets/meal_item.dart';

class CategoryMealsScreen extends StatefulWidget {
  static const routName = '/categories';
  final List<Meal> availableMeals;

  CategoryMealsScreen(this.availableMeals);

  @override
  _CategoryMealsScreenState createState() => _CategoryMealsScreenState();
}

class _CategoryMealsScreenState extends State<CategoryMealsScreen> {
  String categoryTitle;
  List<Meal> categoryList;
  var _loadInitData = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (!_loadInitData) {
      final routeArgs =
          ModalRoute.of(context).settings.arguments as Map<String, String>;
      categoryTitle = routeArgs['title'];
      final categoryId = routeArgs['id'];
      categoryList = widget.availableMeals.where((mealItem) {
        return mealItem.categories.contains(categoryId);
      }).toList();
      _loadInitData = true;
    }
    super.didChangeDependencies();
  }

  void _removeMeal(String mealId) {
    setState(() {
      categoryList.removeWhere((element) => element.id == mealId);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          categoryTitle,
        ),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return MealItem(
              mealId: categoryList[index].id,
              title: categoryList[index].title,
              imageUrl: categoryList[index].imageUrl,
              duration: categoryList[index].duration,
              complexity: categoryList[index].complexity,
              affordability: categoryList[index].affordability,
              );
        },
        itemCount: categoryList.length,
      ),
    );
  }
}

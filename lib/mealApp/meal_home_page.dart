import 'package:flutter/material.dart';

import './dumy_data.dart';
import './model/meal.dart';
import './screen/categories_screen.dart';
import './screen/category_meals_screen.dart';
import './screen/filter_screen.dart';
import './screen/meal_details.dart';
import './screen/tabs_screen.dart';

class MealHomePage extends StatefulWidget {
  @override
  _MealHomePageState createState() => _MealHomePageState();
}

class _MealHomePageState extends State<MealHomePage> {
  Map<String, bool> _filters = {
    'gluten': false,
    'lactose': false,
    'vegan': false,
    'vegtearian': false
  };
  List<Meal> _availableMeal = DUMMY_MEALS;

  List<Meal> _favouriteMeal=[];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _filters = filterData;
      _availableMeal = DUMMY_MEALS.where((element) {
        if (_filters['gluten'] == true && !element.isGlutenFree) {
          return false;
        }
        if (_filters['lactose'] == true && !element.isLactoseFree) {
          return false;
        }
        if (_filters['vegan'] == true && !element.isVegan) {
          return false;
        }
        if (_filters['vegetarian'] == true && !element.isVegetarian) {
          return false;
        }
        return true;
      }).toList();
    });
  }

  void _toggleFavourite(String mealId){
    final existingMeal=_favouriteMeal.indexWhere((element) =>element.id==mealId);
    if(existingMeal>=0){
      setState(() {
        _favouriteMeal.removeAt(existingMeal);
      });
    }else{
      setState(() {
        _favouriteMeal.add(DUMMY_MEALS.firstWhere((element) => element.id==mealId));
      });
    }
    
  }

  bool isFavouritMeal(String id){
    return _favouriteMeal.any((element) =>element.id==id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Title',
      //home: MealAppPage(),
      initialRoute: '/', // default is '/'
      routes: {
        //othe way to load main screen
        '/': (context) => TabsScreen(_favouriteMeal),
        CategoryMealsScreen.routName: (context) =>
            CategoryMealsScreen(_availableMeal),
        MealDetailScreen.routName: (context) => MealDetailScreen(_toggleFavourite,isFavouritMeal),
        FilterScreen.rountName: (context) => FilterScreen(_filters,_setFilters)
      },
      //if you not define the rout then you will seet the defalut rout paage using onGenerateRoute
      onGenerateRoute: (setting) {
        print(setting.arguments);
        // return MaterialPageRoute(builder: (ctx) => MealHomePage());
      },
      //same as onGenerteRoute unknow page direction
      onUnknownRoute: (settings) {
        return MaterialPageRoute(builder: (ctx) => MealHomePage());
      },
      theme: ThemeData(
        primaryColor: Colors.pink,
        accentColor: Colors.amber,
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'OpenSans',
        textTheme: ThemeData.light().textTheme.copyWith(
              body1: TextStyle(
                color: Color.fromRGBO(
                  20,
                  51,
                  51,
                  1,
                ),
              ),
              body2: TextStyle(
                  color: Color.fromARGB(
                20,
                51,
                51,
                1,
              )),
              title: TextStyle(
                fontSize: 20,
                fontFamily: 'OpenSans',
                fontWeight: FontWeight.bold,
              ),
            ),
      ),
    );
  }
}

class MealAppPage extends StatefulWidget {
  @override
  _MealAppPageState createState() => _MealAppPageState();
}

class _MealAppPageState extends State<MealAppPage> {
  @override
  Widget build(BuildContext context) {
    final mAppbar = AppBar(
      title: Text('Daily Meal ',
          style: TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
          textAlign: TextAlign.center),
    );
    final mBody = CategoriesScreen();

    return Scaffold(
      appBar: mAppbar,
      body: mBody,
    );
  }
}

import 'package:flutter/material.dart';

import '../screen/filter_screen.dart';

class MainDrawer extends StatelessWidget {
  Widget builDrawerMenu(
      String menuName, IconData menuIcon, Function menuClick) {
    return ListTile(
      leading: Icon(
        menuIcon,
        size: 26,
      ),
      title: Text(
        menuName,
        style: TextStyle(
          fontSize: 24,
          fontWeight: FontWeight.bold,
          color: Colors.black38,
        ),
      ),
      onTap: menuClick,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Column(
        children: [
          Container(
            color: Theme.of(context).accentColor,
            height: 130,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            child: Text(
              "Cooking Up!",
              style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 40,
                color: Theme.of(context).primaryColor,
              ),
            ),
          ),
          SizedBox(
            height: 20,
          ),
          builDrawerMenu('Restaurant', Icons.restaurant, () {
            Navigator.of(context).pushReplacementNamed('/');
          }),
          builDrawerMenu('Filter', Icons.filter_list, () {
            Navigator.of(context).pushReplacementNamed(
              FilterScreen.rountName,
            );
          }),
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app/mealApp/model/meal.dart';
import 'package:flutter_app/mealApp/screen/meal_details.dart';

class MealItem extends StatelessWidget {
  final String mealId;
  final String title;
  final String imageUrl;
  final int duration;
  final Complexity complexity;
  final Affordability affordability;

  MealItem(
      {@required this.mealId,
      @required this.title,
      @required this.imageUrl,
      @required this.duration,
      @required this.complexity,
      @required this.affordability,
      });

  String get compelexitiyText {
    switch (complexity) {
      case Complexity.Simple:
        return 'Simple';
      case Complexity.Challenging:
        return 'Challenging';
      case Complexity.Hard:
        return 'Hard';
      default:
        return 'Unknow';
    }
  }

  String get affortablitiyText {
    switch (affordability) {
      case Affordability.Affordable:
        return 'Affordable';
      case Affordability.Pricey:
        return 'Pricey';
      case Affordability.Luxurious:
        return 'Luxurious';
      default:
        return 'Unknow';
    }
  }

  void selectMealItem(BuildContext mContext) {
    Navigator.of(mContext).pushNamed(
      MealDetailScreen.routName,
      arguments: mealId,
    ).then((value) => {
      if(value!=null){
       // removeItem(value)
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () => selectMealItem(context),
      child: Card(
        elevation: 4,
        margin: EdgeInsets.all(10),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          children: <Widget>[
            Stack(
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  ),
                  child: Image.network(
                    imageUrl,
                    height: 250,
                    width: double.infinity,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                  bottom: 20,
                  right: 10,
                  child: Container(
                    width: 300,
                    color: Colors.black54,
                    padding: EdgeInsets.symmetric(vertical: 5, horizontal: 20),
                    child: Text(
                      title,
                      style: TextStyle(
                        fontSize: 26,
                        color: Colors.white,
                      ),
                      softWrap: true,
                      overflow: TextOverflow.fade,
                    ),
                  ),
                )
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(20),
              child: MealItemBottom(
                  duration: duration,
                  compelexitiyText: compelexitiyText,
                  affortablitiyText: affortablitiyText),
            )
          ],
        ),
      ),
    );
  }
}

class MealItemBottom extends StatelessWidget {
  const MealItemBottom({
    Key key,
    @required this.duration,
    @required this.compelexitiyText,
    @required this.affortablitiyText,
  }) : super(key: key);

  final int duration;
  final String compelexitiyText;
  final String affortablitiyText;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Row(
          children: [
            Icon(Icons.schedule),
            SizedBox(height: 6),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text('${duration} min'),
            ),
          ],
        ),
        Row(
          children: [
            Icon(Icons.work),
            SizedBox(height: 6),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(compelexitiyText),
            ),
          ],
        ),
        Row(
          children: [
            Icon(Icons.attach_money),
            SizedBox(height: 6),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Text(affortablitiyText),
            ),
          ],
        )
      ],
    );
  }
}
